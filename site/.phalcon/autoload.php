<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
define('AUTOLOAD_PATH', __DIR__ . '/../app/config/');
include BASE_PATH . DS . 'vendor/autoload.php';
$dotEnv = new Dotenv\Dotenv(AUTOLOAD_PATH);
$dotEnv->load();