<table id="example"  class="table table-bordered table-striped">

    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Operation</th>
    </thead>

    <tbody>
    </tbody>

</table>

 
{{ partial("_partials/js") }}
  <script type="text/javascript">
      
 $(document).ready(function() {
                $('#example').DataTable({
                    serverSide: true,
                    ajax: {
                        url: '/crudgen/allModules',
                        method: 'POST'
                    },
                    columns: [
                        {data: "id", searchable: false},
                        {data: "name"},
                        {data: "operation"}
                    ]
                });
            });
</script> 



                                
 
