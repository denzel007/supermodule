<div class="page-header">
    <h1>Congratulations!</h1>
</div>

<p>You're now flying with Phalcon. Great things are about to happen!</p>
 {%- if not(logged_in is empty) %}
            {{ link_to('supermodule', 'Supermodule') }}
            <br>
            {{ link_to('crudgen', 'Crudgen') }}   <br>
            {{ link_to('dashboard', 'Dashboard') }}   <br>
            {{ link_to(config.modules.authreg.logoutLink, 'Logout') }}

  {% else %}
            {{ link_to('authreg/login', 'Login') }}
            
  {% endif %}


<p>This page is located at <code>views/index/index.volt</code></p>
 {{flash.output()}}
