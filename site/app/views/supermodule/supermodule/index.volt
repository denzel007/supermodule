<table id="example"  class="table table-bordered table-striped">

    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Status</th>
        <th>Operation</th>
    </thead>

    <tbody>
    </tbody>

</table>

 
{{ partial("_partials/js") }}
  <script type="text/javascript">
      
 $(document).ready(function() {
                $('#example').DataTable({
                    serverSide: true,
                    ajax: {
                        url: '/supermodule/allModules',
                        method: 'POST'
                    },
                    columns: [
                        {data: "id", searchable: false},
                        {data: "name"},
                        {data: "status"},
                        {data: "operation"}
                    ]
                });
            });
</script> 
   
                                
 
