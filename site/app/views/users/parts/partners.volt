<select class="selectpicker" data-live-search="true" name="condition[{{ line }}][companyId]" >
    {% for company in companies %}
        <option value="{{ company.id }}" {% if condition is not empty and condition['companyId'] == company.id %}selected="selected"{% endif %} >{{ company.name }}</option>
    {% endfor %}
</select>
