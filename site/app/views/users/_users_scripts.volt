<script type="text/javascript">
    function usersIndexScripts()
    {
        initLimit();

        //disable change add condition label
        $('.orange_select .btn-group ul li').click(function(){
            var that = $(this);
            $('.orange_select button').trigger('click');
            addCondition(that.data('original-index'), that.closest('form'));

            return false;
        });

        $('body').on('change', 'select.selectpicker', function(){
            var that = $(this);
            setTimeout(function(){ segmentUsersCount(that.closest('form')); }, 300);
        });

        $('.createSegment').click(function(){
            $('.select_group').remove();
        });

        $(".editsegment").click(function(){
            loadSegmentForEdit($(this).data('id'));
        });

        $('.deletesegment').click(function(){
            $('input[name="removeid"]').val($(this).data('id'));
        });

        $('.destroysegment').click(function(){
            var params = {'id': $('input[name="removeid"]').val()};

            $.ajax({
                url: baseUrl + 'users/deleteSegment',
                type: 'post',
                data: params,
                success: function (answer) {
                    if(answer.success){
                        $('#segment' + params['id']).remove();
                    }
                }
            });
        });

        $('.check_all_in_pannel').find('input[type="checkbox"]').change(function(){
            $('.users_container').prop('checked', true);
        });

        $('#createsegment, #editsegmentForm').each(function(){
            $(this).validate({
                ignore: '.ignore',
                rules: {
                    title: {
                        required: true
                    }
                },
                submitHandler : function(obj) {
                    var form = $(obj),
                        conditions = [];

                        form.find('.select_group').each(function(){
                            var that = $(this),
                                ln = that.data('line'),
                                type = that.find('select[name="condition[' +ln+ '][type]"]').val();

                            if (type == 1) {
                                conditions[conditions.length] = {
                                    'type': type,
                                    'purchased': that.find('select[name="condition[' +ln+ '][purchased]"]').val(),
                                    'dealId': that.find('select[name="condition[' +ln+ '][dealId]"]').val()
                                }
                            } else {
                                conditions[conditions.length] = {
                                    'type': type,
                                    'visited': that.find('select[name="condition[' +ln+ '][visited]"]').val(),
                                    'skiStationId': that.find('select[name="condition[' +ln+ '][skiStationId]"]').val(),
                                    'companyId': that.find('select[name="condition[' +ln+ '][companyId]"]').val()
                                }
                            }                            
                        });

                    params = {
                            'id': form.find('input[name="id"]').val(),
                            'title': form.find('input[name=title]').val(),
                            'conditions': conditions
                        }

                    $.ajax({
                        url: baseUrl + 'users/saveSegment',
                        type: 'post',
                        data: params,
                        success: function (answer) {
                            if(answer.success){
                                if ($(form).attr('name') == 'createsegment') {
                                    form.find('input').val('');
                                    $('div.segment_box div.row').append(segmentBox(answer.segment.id, answer.segment.title, 75));
                                    $('#createSegment').modal('hide');
                                    $(".segment_box").on("click", ".editsegment", function () {
                                        loadSegmentForEdit(answer.segment.id);
                                        $('#editSegment').modal('show');
                                    });
                                    $(".segment_box").on("click", ".deletesegment", function () {
                                        $('input[name="removeid"]').val(answer.segment.id);
                                        $('#deleteModal').modal('show');
                                    });
                                } else {
                                    $('#segment' + answer.segment.id).find('.inner_info p').first().html(answer.segment.title);
                                    if ($('select[name="changeSegment"]').length) {
                                        window.location = $('select[name="changeSegment"]').val();
                                        return false;
                                    }
                                    $('#editSegment').modal('hide');
                                }   
                                $('#segment' +answer.segment.id).find('div.inner_info p').last().html(form.find('span.users_count').html() + ' users');
                            }
                        }
                    });
                    return false;
                }
            });
        });

        $('.checkAll').on('click', function() {
            if($(this).hasClass('checked')){
                $('.userCheck').each(function(){
                    if(!$(this).hasClass('checked')){
                        $(this).trigger('click');
                    }
                });
            }else{
                $('.userCheck').each(function(){
                    if($(this).hasClass('checked')){
                        $(this).trigger('click');
                    }
                });
            }
        });

        $('.userCheck').on('click', function(){
            showMassActions();
        });

        $('.massReset').on('click', function(){
            $('#resetPassword .message_alert').html('Send link reset password for all selected users?');
            $('input[name=resetType]').val('mass');
            $('input[name=id]').val('0');
        });

        $('.resetUser').click(function(){
            $('#resetPassword .message_alert').html('Send link reset password for this user?');
            $('input[name=resetType]').val('one');
            $('input[name=id]').val($(this).data('id'));
            $('#resetPassword').modal('show');
        });

        $('.reset').click(function(){
            if($('input[name=resetType]').val() == 'one'){
                resetPassword([$('input[name=id]').val()]);
            }else{
                var users = [];
                $('.userCheck').each(function(){
                    if($(this).hasClass('checked')){
                        users[users.length] = $(this).data('id');
                    }
                });

                resetPassword(users);
            }

            $('#resetPassword').modal('hide');
        });

        $('.massDelete').click(function(){
            $('#deleteModalLabel').html('Delete all selected users?');
            $('input[name=removeType]').val('mass');
            $('input[name=removeid]').val('0');
        });

        $('.removeUser').click(function(){
            $('#deleteModalLabel').html('Delete this user?');
            $('input[name=resetType]').val('one');
            $('input[name=removeid]').val($(this).data('id'));
            $('#deleteUser').modal('show');
        });

        $('.destroyuser').click(function(){
            if($('input[name=removeType]').val() == 'one'){
                deleteUsers([$('input[name=removeid]').val()]);
            }else{
                var users = [];
                $('.userCheck').each(function(){
                    if($(this).hasClass('checked')){
                        users[users.length] = $(this).data('id');
                    }
                });

                deleteUsers(users);
            }

            $('#deleteUser').modal('hide');
        });
    }

    function usersShowScripts()
    {
        $('.resetBtn').click(function(){
            $('#resetPassword .message_alert').html('Send link reset password for this user?');
            $('input[name=resetType]').val('one');
            $('input[name=id]').val($(this).data('id'));
        });

        $('.reset').click(function(){
            resetPassword([$('input[name=id]').val()]);
            $('#resetPassword').modal('hide');
        });
    }

    var line = 1;
    function addCondition(conditionType, form, condition = null)
    {
        var params = {
            'conditionType': conditionType,
            'line': line
        };

        if(condition){
            params['condition'] = condition;
        }

        $.ajax({
            url: baseUrl + 'users/getConditionsLine',
            type: 'post',
            data: params,
            success: function (answer) {
                console.log(answer);
                if(answer.success){
                    form.find('.orange_select').before('<div class="select_group condition_line' +params.line+ '" data-line="' +params.line+ '" >' + answer.condition + '</div>');
                    prepareCondition($('.condition_line' +params.line));
                }                
            }
        });
        line++;
    }


    function prepareCondition(group)
    {
        group.find('.selectpicker').selectpicker('refresh');
        group.find('.round_close').click(function(){
            var form = $(this).closest('form');
            setTimeout(function(){ segmentUsersCount(form); }, 300);
            group.remove();
        });
        group.find('.changer').change(function(){
            if($(this).is('select')){
                reloadCondition(group, $(this).val());
            }
        });
        group.find('select.skiStation').on('change', function () {
            changeSkiStation(group);
        });
    }

    function reloadCondition(group, conditionType)
    {
        var params = {
            'conditionType': conditionType,
            'line': group.data('line')
        };

        $.ajax({
            url: baseUrl + 'users/getConditionsLine',
            type: 'post',
            data: params,
            success: function (answer) {
                if(answer.success){
                    group.html(answer.condition);
                    prepareCondition(group);
                }
            }
        });
    }

    function changeSkiStation(group)
    {
        var partnersBlock = group.find('div.partnersBlock'),
            params = {
                'skiStation': group.find('select.skiStation').val(),
                'line': group.data('line')
            };

        $.ajax({
            url: baseUrl + 'users/getPartners',
            type: 'post',
            data: params,
            success: function (answer) {
                partnersBlock.html('');
                if(answer.success){
                    partnersBlock.html(answer.partners);
                    group.find('.selectpicker').selectpicker('refresh');                    
                }
            }
        });
    }

    function showMassActions()
    {
        if($('.userCheck.checked').length){
            $('.massActions').removeClass('hide');
        }else{
            $('.massActions').addClass('hide');
        }
    }

    function resetPassword(users)
    {
        if(users.length){
            var modal = $('#messageModal');

            modal.find('.modal-title').html('Password reset');

            $.ajax({
                url: baseUrl + 'users/resetPassword',
                type: 'post',
                data: {'users': users},
                success: function (answer) {
                    if(answer.success){                        
                        modal.find('.message_alert').html('Link for reset password sended to email');                        
                    }else{
                        modal.find('.message_alert').html(answer.message);
                    }
                    modal.modal('show');
                }
            });
        }
    }

    function deleteUsers(users)
    {
        if(users.length){
            $.ajax({
                url: baseUrl + 'users/delete',
                type: 'post',
                data: {'users': users},
                success: function (answer) {
                    if(answer.success){
                        for(var i = 0; i < users.length; i++){
                            $('div[data-userow=' + users[i] + ']').remove();
                        }
                    }else{

                    }
                }
            });
        }
    }

    function segmentBox(id, title, users)
    {
        var html = '<div class="col-md-2" id="segment' +id+ '">';
        html += '   <div class="segment_block clickable-row" data-href="{{ url('users/segment/') }}' +id+ '">';
        html += '       <div class="btn-group dubble_dropdown">';
        html += '           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        html += '               <div class="boxCheckbox">';
        html += '                   <div class="check">';
        html += '                       <i class="fa fa-square-cube disabled_icon"></i>';
        html += '                       <i class="fa fa-check-square ACTIVE_icon"></i>';
        html += '                       <input id="" type="checkbox" class="actualCheckItem" name="segment" value="' +id+ '"/>';
        html += '                   </div>';
        html += '               </div>';
        html += '               <i class="fa fa-caret-down" aria-hidden="true"></i>';
        html += '           </button>';
        html += '           <div class="dropdown-menu screen_menu">';
        html += '               <a class="dropdown-item editsegment" data-toggle="modal" data-action="edit_modal" data-id="' +id+ '" >Edit segment</a>';
        html += '               <a class="dropdown-item red_color deletesegment" data-id="' +id+ '" data-action="delete">Delete</a>';
        html += '           </div>';
        html += '       </div>';
        html += '       <div class="inner_info">';
        html += '           <p>' +title+ '</p>';
        html += '           <p>' +users+' users</p>';
        html += '       </div>';
        html += '   </div>'
        html += '</div>';
        return html;
    }

    function loadSegmentForEdit(id)
    {
        console.log('loadSegmentForEdit called');
        $('.preloader').show();
        var form = $('#editsegmentForm'),
            params = {'id': id};

            console.log('loadSegmentForEdit params' + params.id);
        $.ajax({
            url: baseUrl + 'users/getSegment',
            type: 'post',
            data: params,
            success: function (answer) {
                console.log(answer);
                if(answer.success){
                    form.find('input[name="title"]').val(answer.data.segment.title);
                    form.find('input[name="id"]').val(answer.data.segment.id);
                    form.find('span.users_count').html(answer.data.users);
                    
                    $('.select_group').remove();

                    if (answer.data.skistations) {
                        for (var i = 0; i < Object.keys(answer.data.skistations).length; i++) {
                            addCondition(2, form, answer.data.skistations[i]);
                        }
                    }

                    if (answer.data.deals) {
                        for (var i = 0; i < Object.keys(answer.data.deals).length; i++) {
                            addCondition(1, form, answer.data.deals[i]);
                        }
                    }

                    setTimeout(function(){
                        $('.preloader').hide();
                    }, 1500);
                }
            }
        });
    }

    function segmentUsersCount(form)
    {
        console.log('segmentUsersCount called');
        var conditions = [];

            form.find('.select_group').each(function(){
                var that = $(this),
                    ln = that.data('line'),
                    type = that.find('select[name="condition[' +ln+ '][type]"]').val();

                if (type == 1) {
                    conditions[conditions.length] = {
                        'type': type,
                        'purchased': that.find('select[name="condition[' +ln+ '][purchased]"]').val(),
                        'dealId': that.find('select[name="condition[' +ln+ '][dealId]"]').val()
                    }
                } else {
                    conditions[conditions.length] = {
                        'type': type,
                        'visited': that.find('select[name="condition[' +ln+ '][visited]"]').val(),
                        'skiStationId': that.find('select[name="condition[' +ln+ '][skiStationId]"]').val(),
                        'companyId': that.find('select[name="condition[' +ln+ '][companyId]"]').val()
                    }
                }                            
            });

        if (conditions.length) {
            $.ajax({
                url: baseUrl + 'users/segmentUsersCount',
                type: 'post',
                data: {'conditions': conditions},
                success: function (answer) {
                    console.log(answer);
                    if(answer.success){
                        form.find('.users_count').html(answer.users);
                    }
                }
            });
        } else {
            form.find('.users_count').html(0);
        }
    }

    function segmentScripts(){
        $('select[name=changeSegment]').change(function (){
            window.location = $(this).val();
            return false;
        });
    }
</script>
