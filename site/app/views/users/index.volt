{% extends "base.volt" %}
{% block content %}
    <form method="" action="" id="" name="">
        <div class="page_heading">
            <h1>USERS</h1>
            <div class="top_buttons one_type_buttons">
                <div class="btn-group hide massActions">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-list" aria-hidden="true"></i>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu single_menu screen_menu">
                        <a class="dropdown-item massReset" data-toggle="modal" data-target="#resetPassword" >Reset password</a>
                        <a class="dropdown-item massDelete" data-toggle="modal" data-target="#deleteUser" >Delete</a>
                    </div>
                </div>
                <button type="button" class="btn btn-warning createSegment" data-toggle="modal" data-target="#createSegment">CREATE SEGMENT</button>
            </div>
        </div>
        <h3 class="sub_heading">SEGMENTS</h3>
        <div class="segment_box">
            <div class="row">

                {% if segments is defined %}
                    {% for segment in segments %}
                        <div class="col-md-2" id="segment{{ segment.id }}">
                            <div class="segment_block clickable-row" data-href='{{ url('users/segment/' ~ segment.id) }}'>
                                <div class="btn-group dubble_dropdown">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div class="boxCheckbox">
                                            <div class="check">
                                                <i class="fa fa-square-cube disabled_icon"></i>
                                                <i class="fa fa-check-square ACTIVE_icon"></i>
                                                <input id="" type="checkbox" class="actualCheckItem" name="segment" value=""/>
                                            </div>
                                        </div>
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </button>
                                    <div class="dropdown-menu screen_menu">
                                        <a class="dropdown-item editsegment" data-toggle="modal" data-action="edit_modal" data-id="{{ segment.id }}" >Edit segment</a>
                                        <a class="dropdown-item red_color deletesegment" data-id="{{ segment.id }}" data-action="delete">Delete</a>
                                    </div>
                                </div>
                                <div class="inner_info">
                                    <p>{{ segment.title }}</p>
                                    <p>{{ segment.getUsers()|length }} users</p>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                {% endif %}
            </div>

            <h3 class="sub_heading">USERS ({{ users.total_items }})</h3>
            <div class="page_container users_container">
                <div class="table default_table users_table">
                    <div class="table_row table_heading">
                        <div class="table_cell">
                            <div class="checkbox_body">
                                <div class="boxCheckbox checkAll">
                                    <div class="check check_all_users">
                                        <i class="fa fa-square-cube disabled_icon"></i>
                                        <i class="fa fa-check-square ACTIVE_icon"></i>
                                        <input id="" type="checkbox" class="actualCheckItem" name="" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table_cell">USER NAME</div>
                        <div class="table_cell">EMAIL</div>
                        <div class="table_cell">SEX</div>
                        <div class="table_cell">&nbsp;</div>
                    </div>

                    {% if users.items is defined %}
                        {% for user in users.items %}
                            <div class="table_row clickable-row" data-userow="{{ user.id }}" data-href='{{ url('users/show/' ~ user.id)  }}' >
                                <div class="table_cell">
                                    <div class="btn-group dubble_dropdown sub_panel_dropdown">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <div class="boxCheckbox userCheck" data-id="{{ user.id }}">
                                                <div class="check">
                                                    <i class="fa fa-square-cube disabled_icon"></i>
                                                    <i class="fa fa-check-square ACTIVE_icon"></i>
                                                    <input type="checkbox" class="actualCheckItem" name="userId" value="{{ user.id }}" />
                                                </div>
                                            </div>
                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu screen_menu">
                                            <a class="dropdown-item resetUser" data-id="{{ user.id }}" >Reset password</a>
                                            <a class="dropdown-item removeUser red_color" data-id="{{ user.id }}" >Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table_cell">{{ user.name}} {{ user.surname }}</div>
                                <div class="table_cell">{{ user.email }}</div>
                                <div class="table_cell">{{ user.gender }}</div>
                                <div class="table_cell">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        {% endfor  %}
                    {% else %}
                        <div class="table_row clickable-row" data-href='{{ url('users') }}' >
                            <div class="table_cell"></div>
                            <div class="table_cell">Users not found</div>
                            <div class="table_cell"></div>
                            <div class="table_cell"></div>
                            <div class="table_cell"></div>
                            <div class="table_cell"></div>
                            <div class="table_cell"></div>
                        </div>
                    {% endif %}

                </div>
            </div>
        </div>
    </form>

    {%  include "_partials/pagination"
        with [
            'paginator' : users,
            'page' : page,
            'path' : url('users/', {
                'page' : ''
            })
        ]
    %}

    {% include '_modals/users/segment_create.volt' %}
    {% include '_modals/users/segment_edit.volt' %}
    {% include '_modals/users/segment_delete.volt' %}
    {% include '_modals/users/user_delete.volt' %}
    {% include '_modals/reset_password.volt' %}
    {% include '_modals/message.volt' %}

{% endblock %}

{% block js %}
{# JS #}
    {% include 'users/_users_scripts.volt' %}
    <script>$(document).ready(function(){ usersIndexScripts(); });</script>
{% endblock %}
