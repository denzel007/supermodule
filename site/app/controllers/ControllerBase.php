<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use Tetrapak07\Pager\Pager;
use Tetrapak07\FlashMessage\FlashMessage;
use Tetrapak07\Multilang\MultilangBase;
use Tetrapak07\Multitimezone\TimeZoneBase;
use Phalcon\Mvc\Dispatcher;
use Tetrapak07\Roles\RolesBase;

class ControllerBase extends Controller
{
    public $multilang = null;
    
    public function initialize()
    {
       /* if (!$this->session->has('auth-identity')) {
           $this->response->redirect($this->config->modules->authreg->loginLink);
        }*/
        $timezone = new TimeZoneBase();
        $this->timezone = $timezone;
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    }
    
    public function onConstruct()
    {
        # TO DO check access
        
        /*if (!$this->session->has('auth-identity')) {
           $this->response->redirect($this->config->modules->authreg->loginLink);
        }*/
        $this->beforeExecuteRoute($this->dispatcher);
        
        if (!$this->checkAndConnectModule()) {

           return $this->response->redirect('/error/show404');
        } 
        
        $this->multilang = new MultilangBase();
       
    }
    
     /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
       $rolesAnsAcceses = new RolesBase();
       
       return $rolesAnsAcceses->beforeExecuteRoute($dispatcher);
    }
    
    protected function checkAndConnectModule($controller = false)
    {
        if (!$controller) {
           $controller = $this->router->getControllerName();
        }
        
        return SupermoduleBase::checkAndConnectModule($controller);

    }
    
    public function show404Action()
    {
        
    }
    
    public function show503Action()
    {
        
    }
    
    public function paginator($content, $contentType = '', $limit = false, $currentPage = 1) {
       $pager = new Pager();
       $pager->initialize();
       return $pager->paginator($content, $contentType, $limit, $currentPage);
    }
    
    public function message($type = 'error', $mess = "Not found!", $dataUrl = '', $redirect = false)
    {
       $message = new FlashMessage();
       $message->initialize();
       return $message->message($type, $dataUrl, $mess, $redirect);
    }
    
    public function checkAccess()
    {
        if (!$this->session->has('auth-identity')) {
           $this->response->redirect($this->config->modules->authreg->loginLink);
        }
    }
    
}
