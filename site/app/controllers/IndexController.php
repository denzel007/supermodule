<?php

namespace App\Controllers;

//use Supermodule\SupermoduleController;
use App\Helpers\MailHelper;
use App\Helpers\UploadHelper;
use App\Requests\Filters\TestFilter;
use App\Requests\Validators\TestValidation;
use Tetrapak07\Pager\Pager;
use Tetrapak07\Search\BaseSearch;
use Tetrapak07\Companies\CompaniesBase;
use Tetrapak07\Multilang\MultilangBase;
use App\Helpers\DateHelper;

class IndexController extends ControllerBase
{

    function onConstruct()
    {
        parent::onConstruct();   
    }
    
    public function indexAction()
    {
         # test mail helper
         # return MailHelper::renderOut([], 'emails/test', ['user' => '', 'link' => '']);
         # end test mail helper
        
         # test upload
         # $this->view->pick('upload/test');
         # end test upload
        
         # pager test
         /*$orderBy = 'dateUpdated';

          $orderType = 'asc';
          $news = $this->di->getModelsManager()
                ->createBuilder()
                ->columns(array('App\Models\News.id', 'title', 'dateUpdated', 'DATE_FORMAT(dateUpdated, "%d %M %Y") as dateUpdatedStr'))
                ->from('App\Models\News')
                ->join('App\Models\NewsInfo', 'App\Models\NewsInfo.newsId = App\Models\News.id')
                ->where("companyId = '" . 1 . "'")
                ->orderBy($orderBy . ' ' . $orderType)
                ->limit(100)
                ->getQuery()
                ->execute();
         $currentPage = $this->request->get('page', 'int', 1); 
        
        $data = $this->paginator($news, 'news', false, $currentPage);
        print_r($data);exit;*/
        # end pager test
        
        # test message
        # $this->message();
        # $this->message('success',  $mess  = 'Good!', '/index/show404', $redirect = true);
        # end pager test
        
        # test validation and filtration
        /*$this->testFilter = new TestFilter();
        $this->testValidator = new TestValidation();
        
        $testArr = ['param1' => 1];
        $messages = $this->testValidator->validate($testArr);
        if ($this->testValidator->show($messages)) {
            echo ' show ';
        }
        
        $this->filter = $this->testFilter->filter($this->request, 'currentPage', ['int'], 1);
        echo 'curr page: '.$this->filter->currentPage;//exit; */
        # end test validation and filtration
        
        #test search module
        /*$search = new BaseSearch();
        $searchString = 'test';
        $searchResult = $search->search($searchString, 2);
        echo '$searchResult: '; print_r($searchResult);exit;*/
        #end test search module
        
        #test companies
        # return $this->response->redirect('companies');
        #end test companies module
        
        #test multilang
        /*echo 'lang: '.$this->multilang->bestLang;   
        if ($this->multilang->translation) {
        
        echo '; key: '.$this->multilang->translation->offsetGet('test');
        echo '; key with entyty: '.$this->multilang->translation->offsetGet('about', 'companies', 1);
        $testData = ['companyId' => 2, 'info'=> ['about' => 'Тест о', 'bonus'=> 'Бонус', 'privacy'=> 'пвврв', 'term' => 'Соглашение']];
        $companyId = 2;
        $entity = 'companies';
        foreach ($testData['info'] as $key => $info) {
           echo '<br>'.$key.': '.$info;//($translateKey, $message, $type = '', $id = 0)
           $this->multilang->translation->offsetSet($key, $info, $entity, $companyId);
        }
        echo '<br>';
        $ret = $this->multilang->translation->offsetGetAll('companies', $id = 2);
        print_r($ret);
        }
        exit;*/
        #end test multilang module
        
        # multitime zones test
        /* echo 'default time zone UTC: '.$this->timezone->defaultTimeZoneUTC;
         echo '; user time zone: '.$this->timezone->userTimeZone;
         echo '; user time zone UTC: '.$this->timezone->userTimeZoneUTC;
         $serverDateTime = DateHelper::dateTimeZoneFormat();
         $localeDateTime = DateHelper::dateTimeZoneFormat($serverDateTime, true);
         echo '; $serverDateTime: '.$serverDateTime.'; $localeDateTime: '.$localeDateTime; 
         exit;*/
        # end multitime zones test
        
        #authreg  test
         //return $this->response->redirect('authreg/login');
        # end authreg zones test
       // $html = MailHelper::renderOut([], 'authreg/emailTemplates/confirmation', ['user' => 'user', 'confirmUrl' => 'link']);
       // echo $html;exit;
      // MailHelper::sendMailOut('tetrapak07@gmail.com', "Please confirm your email", $html, 'Company Name');
    }
    
    public function testUploadAction()
    {
       $imgData = UploadHelper::uploadImage($this->request);
       print_r($imgData);exit;
    }        
    
    
}

