<?php

namespace App\Controllers;

use  App\Controllers\ControllerBase;
use Phalcon\Mvc\Controller;

class GendocController extends Controller
{

    
    function onConstruct()
    {
       new ControllerBase();
        
    }    
    
    public function indexAction()
    {
        $this->response->redirect('docs');
    } 
    
}
