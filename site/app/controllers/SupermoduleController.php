<?php

namespace App\Controllers;

use \Supermodule\SupermoduleController as BaseSupermodule;
use  App\Controllers\ControllerBase;

class SupermoduleController extends BaseSupermodule
{

    public function initialize()
    {  
      parent::initialize();
      $this->view->setViewsDir($this->config->modules->supermodule->viewsDir); 
    }
    
    function onConstruct()
    {
        new ControllerBase();
        
    }  
    
    public function indexAction()
    {
        parent::indexAction(); 
    }
    
    public function allModulesAction() 
    {
        parent::allModulesAction();  
    }
    
    public function changeAction($status = 'off', $moduleId = false) 
    {
        if (($status == 'off') AND ($moduleId == 18)/*gendoc*/) {
             $content = 'deny from all';
               $f = fopen($this->config->application->publicDir.'docs'.DS.'.htaccess', "w");
               fwrite($f, $content);
               fclose($f);
        } elseif (($status == 'on') AND ($moduleId == 18)/*gendoc*/) {
               $content = 'allow from all';
               $f = fopen($this->config->application->publicDir.'docs'.DS.'.htaccess', "w");
               fwrite($f, $content);
               fclose($f);
        }
        parent::changeAction($status, $moduleId); 
    }
}

