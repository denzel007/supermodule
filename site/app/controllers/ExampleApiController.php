<?php

namespace App\Controllers;

use tetrapak07\Api\UsersApiController;

class ExampleApiController extends UsersApiController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function showAction($id)
    {

    }

    public function saveAction()
    {

    }

    public function loginAction()
    {

    }

    public function socialLoginAction()
    {

    }

    public function emailAction()
    {

    }

    public function registrationAction()
    {

    }

    public function restoreAction()
    {

    }

    public function avatarAction($id)
    {

    }

    public function settingsAction()
    {

    }
}
