<?php

namespace App\Controllers\Crudgen;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Tetrapak07\Roles\RolesBase;
use App\Libs\Acl\Acl;

class ControllerBase extends Controller
{
   function initialize()
   {      
      # check access to CRUD Generator by role ID 
      $roleId = $this->session->get('roleId');
      
      
      $this->view->menu = $this->menuGen();
      $this->view->activeMenu = $this->dispatcher->getControllerName();
      $this->view->crudPart = $this->config->modules->crudgen->crudPart;
      
   } 
   
    function onConstruct()
    {
        //$base = new CB();
        
        //$base->onConstruct();
        
    } 
   
   public function getAttributes($modelInstanse) 
   {           
      $metadata = $modelInstanse->getModelsMetaData();

      return $metadata->getAttributes($modelInstanse);
   }
   
   protected function checkIfCrudExist($crudName, $all = false)
   {
        $files = scandir($this->config->modules->crudgen->viewsDirGen);

        if (in_array($crudName, $files)) {
             
             return true;
        }
        return false;
    }
    
    protected function menuGen() 
    {
      $ret = [];
      
       $tablesQuery = $this->db->query("SHOW TABLES");
         $i = 0;
         while ($table = $tablesQuery->fetch()) {
             if ($this->checkIfCrudExist($table['Tables_in_'.$this->config->database->dbname.''])) {
               $ret[$i] =  $table['Tables_in_'.$this->config->database->dbname.'']; 
               $i++;
             }
             
         }
      
      return $ret; 
    }
    
     /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
       $rolesAndAcceses = new RolesBase();
       
       return $rolesAndAcceses->beforeExecuteRoute($dispatcher);
    }
}
