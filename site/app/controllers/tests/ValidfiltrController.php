<?php

namespace App\Controllers\Tests;

use App\Requests\Filters\TestFilter;
use App\Requests\Validators\TestValidation;

class ValidfiltrController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
        # test validation and filtration
        $this->testFilter = new TestFilter();
        $this->testValidator = new TestValidation();
        
        $testArr = ['param1' => 'ghfgh'];
        $messages = $this->testValidator->validate($testArr);
        if ($this->testValidator->show($messages)) {
            echo ' show ';
           // $this->message('success',  $mess  = 'Good!', '/index/show404', $redirect = true);
        }
        
        $this->filter = $this->testFilter->filter($this->request, 'currentPage', ['int'], 1);
        echo 'curr page: '.$this->filter->currentPage;
        
        # end test validation and filtration 
    }
}
