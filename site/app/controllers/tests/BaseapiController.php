<?php

namespace App\Controllers\Tests;

class BaseapiController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
         $this->response->redirect($this->config->modules->baseapi->apiUrlPart); 
    }
}
