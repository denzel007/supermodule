<?php

namespace App\Controllers\Tests;

class PagerController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
         # pager test
      
          $modules = $this->di->getModelsManager()
                ->createBuilder()
                ->columns(array('App\Models\Modules.id', 'name'))   
                ->from('App\Models\Modules')
                ->getQuery()
                ->execute();
         $currentPage = $this->request->get('page', 'int', 1); 
        
        $data = $this->paginator($modules, 'modules', false, $currentPage);
        print_r($data);
        # end pager test
    }
}