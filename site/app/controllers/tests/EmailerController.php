<?php

namespace App\Controllers\Tests;

use App\Helpers\MailHelper;


class EmailerController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
         # test mail helper
          return MailHelper::renderOut([], 'emails/test', ['user' => '', 'link' => '']);
         # end test mail helper
    }
}
