<?php

namespace App\Controllers\Tests;

use App\Helpers\DateHelper;

class MultitimezoneController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
         # multitime zones test
         echo 'default (server) time zone UTC: '.$this->timezone->defaultTimeZoneUTC;
         echo '; user time zone: '.$this->timezone->userTimeZone;
         echo '; user time zone UTC: '.$this->timezone->userTimeZoneUTC;
         $serverDateTime = DateHelper::dateTimeZoneFormat();
         $localeDateTime = DateHelper::dateTimeZoneFormat($serverDateTime, true);
         echo '; $serverDateTime: '.$serverDateTime.'; $localeDateTime: '.$localeDateTime; 
        # end multitime zones test
    }
}
