<?php

namespace App\Controllers\Tests;

use Tetrapak07\Multilang\MultilangBase;


class MultilangController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
        #test multilang
        echo 'lang: '.$this->multilang->bestLang;   
        if ($this->multilang->translation) {
        
        echo '; key: '.$this->multilang->translation->offsetGet('test');
        echo '; key with entyty: '.$this->multilang->translation->offsetGet('about', 'companies', 1);
        $testData = ['companyId' => 2, 'info'=> ['about' => 'Тест о', 'bonus'=> 'Бонус', 'privacy'=> 'пвврв', 'term' => 'Соглашение']];
        $companyId = 2;
        $entity = 'companies';
        foreach ($testData['info'] as $key => $info) {
           echo '<br>'.$key.': '.$info;//($translateKey, $message, $type = '', $id = 0)
           $this->multilang->translation->offsetSet($key, $info, $entity, $companyId);
        }
        echo '<br>';
        $ret = $this->multilang->translation->offsetGetAll('companies', $id = 2);
        print_r($ret);
        }
        //exit;
        #end test multilang module
    }
}