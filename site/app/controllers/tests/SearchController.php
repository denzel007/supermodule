<?php

namespace App\Controllers\Tests;

use Tetrapak07\Search\BaseSearch;

class SearchController extends \App\Controllers\ControllerBase
{
    public function indexAction()
    {
        #test search module
        $search = new BaseSearch();
        $searchString = 'api';
        $searchResult = $search->search($searchString, 2);
        echo '$searchResult: '; print_r($searchResult);
        #end test search module
    }
}
