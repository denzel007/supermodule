<?php

namespace App\Controllers;

use tetrapak07\Users\UsersBaseController;

abstract class UsersController extends UsersBaseController
{
    public function initialize()
    {

    }

    public function indexAction()
    {

    }

    public function showAction($userId)
    {

    }

    public function resetPasswordAction()
    {

    }

    public function deleteAction()
    {

    }

    public function forgotAction()
    {

    }
}
