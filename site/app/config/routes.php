<?php

/*
 * Define custom routes. File gets included in the router service definition.
 */

$router = new Phalcon\Mvc\Router();

$router->add(
    "/",
    [
        //"namespace"  => "App\\Controllers",
        "controller" => "Index",
        "action"     => "index",
    ]
);

$part = $this->config->modules->crudgen->crudPart;

$router->setDefaultModule("custom");

        
       $router->add(
            "/$part/:controller/:action",
            [
                "controller" => 1,
                "action" => 2,
            ]
        );
       
        $router->add(
            "/$part/:controller/:action/:params",
            [
                "controller" => 1,
                "action" => 2,
                "params" => 3
            ]
        );
        
        $router->add(
            "/$part/:controller",
            [
                "controller" => 1,
            ]
        );

        
return $router;
