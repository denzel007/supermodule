<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces(
    [
       "App\\Controllers" =>  $config->application->controllersDir,
       "App\\Models"      =>  $config->application->modelsDir,
       "App\\Helpers"      => $config->application->helpersDir,
       "App\\Forms"      => $config->application->formsDir,
       "App\\Libs"      => $config->application->libsDir,
       "App\\Libs\\Auth"      => $config->application->libsAuthDir,
       "App\\Requests"      => $config->modules->validfiltr->requestsDir ? $config->modules->validfiltr->requestsDir : $config->application->requestsDir,
       "App\\Requests\Filters"      => $config->modules->validfiltr->filtersDir,
       "App\\Requests\Validators"      => $config->modules->validfiltr->validatorsDir,
       "Baseapi"      => $config->modules->baseapi->baseApiDir,  
       "Baseapi\\V1"      => $config->modules->baseapi->baseApiV1Dir, 
       "Baseapi\\V1\\Controllers"      => $config->modules->baseapi->baseApiV1DirC, 
     ]
);

$loader->register();
