<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') || define('APP_PATH', BASE_PATH . ''.DS.'app');
defined('VENDOR_COMPANY') || define('VENDOR_COMPANY','tetrapak07');
defined('MODULES_PATH') || define('MODULES_PATH', BASE_PATH.DS.'vendor'.DS.'tetrapak07');

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => getenv('DATABASE_HOST'),
        'username'    => getenv('DATABASE_USER'),
        'password'    => getenv('DATABASE_PASS'),
        'dbname'      => getenv('DATABASE_NAME'),
        'charset'     => getenv('DATABASE_CHARSET'),
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH .DS.'controllers'.DS.'',
        'modelsDir'      => APP_PATH .DS.'models'.DS.'',
        'migrationsDir'  => APP_PATH . ''.DS.'migrations'.DS.'',
        'viewsDir'       => APP_PATH . ''.DS.'views'.DS.'',
        'pluginsDir'     => APP_PATH . ''.DS.'plugins'.DS.'',
        'libraryDir'     => APP_PATH . ''.DS.'library'.DS.'',
        'cacheDir'       => BASE_PATH . ''.DS.'cache'.DS.'',
        'defaultNamespace' => 'App\\Controllers',
        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ],
    'app' => [
        'debug' => true,
        'showErrorsPage' => false
    ],
    'templateEngine' => 'volt',
    
    'modules' => [
        
     'supermodule' => [
            'viewsDir'       => BASE_PATH.''.DS.'vendor'.DS.VENDOR_COMPANY.DS.'supermodule'.DS.'site'.DS.'app'.DS.'views',
            'controllersDir' => APP_PATH .DS.'controllers'.DS.'',
            'modelsDir'      => APP_PATH .DS.'models'.DS.'',
           
        ],
        
      'crudgen' => [
            'viewsDir'       => BASE_PATH.''.DS.'vendor'.DS.VENDOR_COMPANY.DS.'crudgen'.DS.'site'.DS.'views'.DS,
            'viewsDirGen'       => APP_PATH . ''.DS.'views'.DS.'crudgen'.DS,
            'controllersDir' => APP_PATH .DS.'controllers'.DS.'crudgen'.DS,
            'modelsDir'      => APP_PATH .DS.'models'.DS.'crudgen'.DS,
            'templatesPath' => 'vendor'.DS.VENDOR_COMPANY.DS.'crudgen'.DS.'site'.DS.'templates'.DS,
            'crudPart' => 'cr',
            'modelsNamespace' => 'App\\Models\\Crudgen',
            'controllersNamespace' => 'App\\Controllers\\Crudgen'
          
        ],   
        
    ],
    
]);

