<?php

use Phalcon\Config; 
use Tetrapak07\Roles\PrivateResourcesBase;

$base = PrivateResourcesBase::getResources();

$custom = [
               'companies' => [
                   'index'
               ]
       ];  

$res = array_merge ($custom, $base);

return new Config([
    'privateResources' => $res
]);

