<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') || define('APP_PATH', BASE_PATH . ''.DS.'app');
defined('VENDOR_COMPANY') || define('VENDOR_COMPANY','tetrapak07');
defined('MODULES_PATH') || define('MODULES_PATH', BASE_PATH.DS.'vendor'.DS.'tetrapak07');

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => getenv('DATABASE_HOST'),
        'username'    => getenv('DATABASE_USER'),
        'password'    => getenv('DATABASE_PASS'),
        'dbname'      => getenv('DATABASE_NAME'),
        'charset'     => getenv('DATABASE_CHARSET'),
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'publicDir'         => BASE_PATH . ''.DS.'public'.DS.'',
        'controllersDir' => APP_PATH .DS.'controllers'.DS.'',
        'requestsDir' => APP_PATH .DS.'requests'.DS.'',
        'modelsDir'      => APP_PATH .DS.'models'.DS.'',
        'helpersDir'      => APP_PATH .DS.'helpers'.DS.'',
        'formsDir'      => APP_PATH .DS.'forms'.DS.'',
        'libsDir'      => APP_PATH .DS.'library'.DS.'',
        'libsAuthDir'      => APP_PATH .DS.'library'.DS.'Auth'.DS.'',
        'migrationsDir'  => APP_PATH . ''.DS.'migrations'.DS.'',
        'viewsDir'       => APP_PATH . ''.DS.'views'.DS.'',
        'pluginsDir'     => APP_PATH . ''.DS.'plugins'.DS.'',
        //'libraryDir'     => APP_PATH . ''.DS.'library'.DS.'',
        'cacheDir'       => BASE_PATH . ''.DS.'cache'.DS.'',
        'defaultNamespace' => 'App\\Controllers',
         'cryptSalt'      => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D',
        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ],
    'app' => [
        'debug'          => getenv('APP_DEBUG'),
        'showErrorsPage' => getenv('APP_SHOW_ERRORS_PAGE'),
        'instVendorConfig' => getenv('INSTALL_VENDOR_CONFIG_FILES'),
        //'showErrorsPage' => true
    ],
    'templateEngine' => 'volt',
    
    'modules' => [
        
     'supermodule' => [
            'viewsDir'       => APP_PATH .DS.'views'.DS.'supermodule',//BASE_PATH.''.DS.'vendor'.DS.VENDOR_COMPANY.DS.'supermodule'.DS.'site'.DS.'app'.DS.'views',
            'controllersDir' => APP_PATH .DS.'controllers'.DS.'',
            'modelsDir'      => APP_PATH .DS.'models'.DS.'',
           
        ],
        
      'crudgen' => [
            'viewsDir'       => APP_PATH .DS.'views'.DS.'crudgen',//BASE_PATH.''.DS.'vendor'.DS.VENDOR_COMPANY.DS.'crudgen'.DS.'site'.DS.'views'.DS,
            'viewsDirGen'       => APP_PATH . ''.DS.'views'.DS.'crudgen'.DS,
            'controllersDir' => APP_PATH .DS.'controllers'.DS.'crudgen'.DS,
            'modelsDir'      => APP_PATH .DS.'models'.DS.'crudgen'.DS,
            'templatesPath' => 'vendor'.DS.VENDOR_COMPANY.DS.'crudgen'.DS.'site'.DS.'templates'.DS,
            'crudPart' => 'cr',
            'modelsNamespace' => 'App\\Models\\Crudgen',
            'controllersNamespace' => 'App\\Controllers\\Crudgen'
          
        ],   
       
      'emailer' => [
        'sender' => 'noreply@your-email.com',
        'senderName' => 'sender name',
        'senderEmail' => 'info@your-domain.com',
        'mandrillApiKey' => 'yourMandrillApiKey',
        'mailChimpApiKey' => 'yourMailChimpApiKey'
       ],
        
      'upload' => [ # Then full upload path = gloabalDir+uploadDir+videoSubDir|imageSubDir
        'gloabalDir' => BASE_PATH . ''.DS.'public'.DS.'',
        'uploadDir' => 'upload',
        'videoSubDir' => 'video',
        'imageSubDir' => 'image',
        'logoWidth' => 500,
        'imgWidth' => 1440
        ],  
        
      'pager' => [
            'limit'       => 10
        ],
        
       'flashmsg' => [
           # add class need for customization standart flash message view that as default "alert alert-xxxx"
           'addClass' => 'flashMess'
        ],
        
       'dashboard' => [
           'viewsDir'       => APP_PATH .DS.'views'.DS.'dashboard', 
        ],
        
        'validfiltr' => [
           'requestsDir' => APP_PATH .DS.'requests'.DS.'',
           'filtersDir' => APP_PATH .DS.'requests'.DS.'filters'.DS.'',
           'validatorsDir' => APP_PATH .DS.'requests'.DS.'validators'.DS.'',
        ],
        'users' => [
            'viewsDir'       => APP_PATH .DS.'views'.DS.'users',
        ],
        
        'search' => [
            'searchModels'       => [
                'App\Models\NewsInfo' => ['title', 'text'],
                'App\Models\Bookings' => ['comment']
            ]
        ],
        
        'companies' => [
          'companyDefault' => 1
        ],
        
        'baseapi' => [
          'apiUrlPart' => 'api/v1',
          'apiNamespace' => 'Baseapi\\V1',
          'baseApiDir' => BASE_PATH . ''.DS.'baseapi'.DS.'',
          'baseApiV1Dir' => BASE_PATH . ''.DS.'baseapi'.DS.'v1'.DS,
          'baseApiV1DirC' => BASE_PATH . ''.DS.'baseapi'.DS.'v1'.DS.'controllers'.DS,  
          //'controllersNamespace' => 'App\\Controllers\\'
            'controllersNamespace' => 'Baseapi\\V1\\Controllers\\'
        ],
        
        'multilang' => [
          'defalultLanguage'       => 'en', 
        ],
        
        'multitimezone' => [
          'defaultTimeZoneUTC'       => 0, #server time
          'defaultTimeZone'       => 'Europe/London', #server time
        ],
        
       'authreg' => [
          'viewsDir'       => APP_PATH .DS.'views'.DS.'authreg',
          'loginLink' => 'authreg/login',
          'logoutLink' => 'authreg/logout', 
          'projectName' => 'Project',
          'title' => 'Auth and Reg',
          'successLoginRedirect' => 'dashboard',
          'publicUrl' => 'general.loc',
          'protocol' => 'http',
          'company' => 'Company',
          'useMail' => true
        ],
        
        'roles' => [
          'redirectUrl' => '/'  
        ] 
        
    ],
    
]);
