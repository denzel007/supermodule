<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\Dispatcher;
use App\Libs\Auth\Auth;
use App\Libs\Acl\Acl;
use Phalcon\Crypt;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    $config = include APP_PATH . ''.DS.'config'.DS.'config.php';
/*
    if (is_readable(APP_PATH . ''.DS.'config'.DS.'config.dev.php')) {
        $override = include APP_PATH . ''.DS.'config'.DS.'config.dev.php';
        $config->merge($override);
    }
*/
    return $config;
});

$config = $di->get('config');

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {


    $url = new UrlResolver();
    //$url->setBaseUri($config->application->baseUri);
    $url->setBaseUri('/');

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {
    //$config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
    //$config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function() use ($config) {
     $classAdd = $config->modules->flashmsg->addClass;
    return new Phalcon\Flash\Session([
       
        "error" => "alert alert-danger ".$classAdd,
        "success" => "alert alert-success ".$classAdd,
        "notice" => "alert alert-info ".$classAdd,
        "warning" => "alert alert-warning ".$classAdd
    ]);
});

/*$di->set(
    "session",
    function () {
        // All variables created will prefixed with "my-app-1"
        $session = new SessionAdapter(
            [
                "uniqueId" => "general",
            ]
        );

        $session->start();

        return $session;
    }
);*/

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Crypt service
 */
$di->set('crypt', function () use ($config) {

    $crypt = new Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

/**
 * Dispatcher use a default namespace
 */
$di->set('dispatcher', function () use ($di) {
    
    $eventsManager = new \Phalcon\Events\Manager();
 
    
    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

        # Handle 404 exceptions
        if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
            $dispatcher->forward(array(
                'controller' => 'error',
                'action' => 'show404'
            ));
            return false;
        }

        # Handle other exceptions
        $dispatcher->forward(array(
            'controller' => 'index',
            'action' => 'show503'
        ));

        return false;
    });
    
    $dispatcher = new Dispatcher();
   if ($di->get('config')->app->showErrorsPage) {      
     $dispatcher->setEventsManager($eventsManager);
    }
    $dispatcher->setDefaultNamespace($di->get('config')->application->defaultNamespace);
    
    return $dispatcher;
});

/**
 * Start the session the first time some component request the session service
 */
/*$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});*/

/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new Auth();
});

/**
 * Setup the private resources, if any, for performance optimization of the ACL.  
 */
$di->setShared('AclResources', function() {
    $pr = [];
    if (is_readable(APP_PATH . '/config/privateResources.php')) {
        $pr = include APP_PATH . '/config/privateResources.php';
    }
    return $pr;
});
/**
 * Access Control List
 * Reads privateResource as an array from the config object.
 */

$db = $di->get('db');
$di->set('acl', function () use ($db, $config) {
    $acl = new Acl();
    $pr = $this->getShared('AclResources')->privateResources->toArray();
    $acl->addPrivateResources($pr);
    $tablesQuery = $db->query("SHOW TABLES");
         while ($table = $tablesQuery->fetch()) {
               $r =  $table['Tables_in_'.$config->database->dbname.'']; 
               if (!in_array($r, $acl->getResources()) AND (!$acl->isAllowed('superadmin', 'crudgen', 'index'))) {
               $acl->addPrivateResources([$r => []]);
               }
         }
    return $acl;
});



/**
 * Loading routes from the routes.php file
 */
$di->set('router', function () use ($config) {
  // return require APP_PATH . ''.DS.'config'.DS.'routes.php';
    $router = new Phalcon\Mvc\Router();

$router->add(
    "/",
    [
        //"namespace"  => "App\\Controllers",
        "controller" => "index",
        "action"     => "index",
    ]
);

$part = $config->modules->crudgen->crudPart;

       $router->add(
            "/$part/:controller/:action",
            [

                "namespace"  => "App\\Controllers\\Crudgen",
                "controller" => 1,
                "action" => 2,
            ]
        );
       
        $router->add(
            "/$part/:controller/:action/:params",
            [

                "namespace"  => "App\\Controllers\\Crudgen",
                "controller" => 1,
                "action" => 2,
                "params" => 3
            ]
        );
        
        $router->add(
            "/$part/:controller",
            [

                "namespace"  => "App\\Controllers\\Crudgen",
                "controller" => 1,
            ]
        );
        
 $partApi = $config->modules->baseapi->apiUrlPart;
 $apiNamespace = $config->modules->baseapi->apiNamespace; 


$router->add(
            "/$partApi/:params",
            [

                "namespace"  => $apiNamespace,
                "controller" => 'api',
                "action" => 'index',
                "params" => 1
                
            ]
 );

        
return $router;
    
});
